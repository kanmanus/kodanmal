<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exhibitions extends Model
{
    use SoftDeletes;
    public $table = 'exhibitions';
    public $primaryKey = 'exhibition_id';
    public $fillable = ['exhibition_name', 'exhibition_date'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
