<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    public $table = 'category';
    public $primaryKey = 'category_id';
    public $fillable = ['title', 'img_name', 'description', 'more_description', 'parent_category_id'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
