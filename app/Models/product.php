<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    public $table = 'product';
    public $primaryKey = 'product_id';
    public $fillable = ['title', 'category_id', 'img_name'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
