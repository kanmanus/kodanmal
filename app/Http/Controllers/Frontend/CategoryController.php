<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use App\Models\Category;
use App\Models\Product;

class CategoryController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function getIndex(){
      // Get All Catgories
      $categories = Category::where('parent_category_id', '0')
                          ->orderBy('category_id', 'asc')->get();

      // Get Sub Categories of each category
      foreach ($categories as $category){
        $category->sub_categories = Category::where('parent_category_id', $category->category_id)
                                            ->orderBy('category_id', 'asc')
                                            ->get();
      }

      return view('frontend.category-list', compact('categories'));
    }

    public function getSubCategories($cat_id){
      // Get the selected category
      $category = Category::find($cat_id);

      // Get All Sub Categories of the selected category
      $sub_categories = Category::where('parent_category_id', $cat_id)
                                  ->orderBy('category_id', 'asc')
                                  ->get();

      return view('frontend.sub-category-list', compact('category', 'sub_categories'));
    }

    public function getSubCategoryDetail($sub_cat_id){
      // Get the selected Sub Category
      $sub_category = Category::find($sub_cat_id);

      // Get the parent category
      $category = Category::find($sub_category->parent_category_id);

      // Get All Products of the selected Sub Category
      $products = Product::where('category_id', $sub_category->category_id)->get();

      return view('frontend.sub-category-detail', compact('category', 'sub_category', 'products'));
    }
}
