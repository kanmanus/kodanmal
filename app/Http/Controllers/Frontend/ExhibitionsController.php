<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use App\Models\Exhibitions;

class ExhibitionsController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function getIndex(){
      // Get All Banners
      $banners = array_diff(scandir('uploads/exhibitions'), array('..', '.'));

      // Get Exhibition Message
      $exhibitions = Exhibitions::orderBy('exhibition_id', 'desc')->get();

      return view('frontend.exhibitions', compact('banners', 'exhibitions'));
    }
}
