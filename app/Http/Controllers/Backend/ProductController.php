<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use Input;

use App\Models\Category;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Product'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Product'; // Page Title
        $this->a_search = ['title']; // Array Search
        $this->path = '_admin/product'; // Url Path
        $this->view_path = 'backend.product.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');

        $data = $obj_model;
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        // Get Category of each Product
        foreach ($data as $product){
          $sub_category = Category::find($product->category_id);
          $category = Category::find($sub_category->parent_category_id);
          $product->category = $category->title . ' > ' . $sub_category->title;
        }

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        // Get All Sub Categories
        $sub_categories = Category::where('parent_category_id', '!=', '0')->get();

        // Prepend parent category's title into the sub category's title
        foreach ($sub_categories as $sub_category){
          $category = Category::find($sub_category->parent_category_id);

          $sub_category->title = $category->title . ' > ' . $sub_category->title;
        }

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','sub_categories'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_model = $this->obj_model;

        $input = $request->all(); // Get all post from form

        $data = $obj_model->create($input);

        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);

        // Get All Sub Categories
        $sub_categories = Category::where('parent_category_id', '!=', '0')->get();

        // Prepend parent category's title into the sub category's title
        foreach ($sub_categories as $sub_category){
          $category = Category::find($sub_category->parent_category_id);

          $sub_category->title = $category->title . ' > ' . $sub_category->title;
        }

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','sub_categories'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_model = $this->obj_model;

        $input = $request->except(['_token','_method','str_param']); // Get all post from form

        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_model = $this->obj_model;
        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
