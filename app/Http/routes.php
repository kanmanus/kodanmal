<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
  'as' => 'home',
  'uses' => 'Frontend\MainController@getHome'
]);

Route::get('about-kodanmal', [
  'as' => 'about-kodanmal',
  'uses' => 'Frontend\MainController@getAboutKodanmal'
]);

Route::get('exhibitions', [
  'as' => 'exhibitions',
  'uses' => 'Frontend\ExhibitionsController@getIndex'
]);

Route::get('contact-us', [
  'as' => 'contact-us',
  'uses' => 'Frontend\MainController@getContactUs'
]);

/* --------------------- Our Products ------------------- */
Route::get('category', [
  'as' => 'category-list',
  'uses' => 'Frontend\CategoryController@getIndex'
]);

Route::get('category/{cat_id}', [
  'as' => 'sub-category-list',
  'uses' => 'Frontend\CategoryController@getSubCategories'
]);

Route::get('sub_category/{sub_cat_id}', [
  'as' => 'sub-category-detail',
  'uses' => 'Frontend\CategoryController@getSubCategoryDetail'
]);

/* --------------------- Admin ------------------- */
Route::group(['prefix' => '_admin'], function () {
    Route::controller('login', 'Backend\LoginController');
    Route::get('logout', 'Backend\LoginController@logout');
});
Route::group(['middleware'=>'admin','prefix' => '_admin'], function () {
    Route::get('/', function () { return view('backend.index'); });
    Route::resource('exhibitions','Backend\ExhibitionsController');
    Route::resource('category','Backend\CategoryController');
    Route::resource('product','Backend\ProductController');
});

/* --------------------- Theme ------------------- */
Route::group(['prefix' => '_theme'], function () {
    Route::get('blank',function(){
        return view('backend.theme_component.blank');
    });
    Route::get('form',function(){
        return view('backend.theme_component.form');
    });
    Route::get('list',function(){
        return view('backend.theme_component.list');
    });
});
