$(function(){
  $('.fadeVisible').addClass("viewport-hidden").viewportChecker({
    classToAdd: 'viewport-visible animated fadeIn',
    offset: 100
  });
});
