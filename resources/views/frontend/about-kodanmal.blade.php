@extends('frontend.layout.main-layout')

@section('title', ' - About KODANMAL')

@section('css')
  {!! Html::style('css/frontend/about-kodanmal.css') !!}
@endsection

@section('content')
  <div id="banner-top">
    <div class="text-center fadeVisible">
      About <span class="strong">KODANMAL</span>
    </div>
  </div>

  <div class="content">
    <div class="container">
      <div class="col-sm-10 col-sm-offset-1">
        <p class="fadeVisible">
          Kodanmal Group has been leading the way in Thai exports for over twenty years.
          We export quality products from Thailand to countries around the world, covering
          an extensive range of product fields while maintaining consistent standards of excellence.
          Companies around the world seeking the best products Thailand has to offer, always look to Kodanmal.
        </p>

        <p class="fadeVisible">
          Situated in the heart of the Bangkok business centre, Thailand, Kodanmal has access
          to a wealth of resources from across the Thai kingdom. By locating, packaging and
          sending these products around the world to tight deadlines, we have built up a loyal
          and satisfied customer base. At present, we specialize in the exportation of more
          than 10 different products from a variety of fields, including food, apparel and
          home decorating items. Seafood, fruit and vegetables, children's clothes, ceramics,
          floorings, and hardboards are our all time best selling goods
        </p>

        <p class="fadeVisible">
          As an exporter, Kodanmal has been able to achieve the confidence of buyers by providing
          quality products at reasonable rates. Our aim is the total satisfaction of our customers.
          Thailand is able to produce almost all kinds of quality products at low costs, and we bring
          this huge resource to our customers.
        </p>

        <p class="fadeVisible">
          In the present business situation, when the market is slashing down, Kodanmal is able not
          only to maintain but also to develop itself at a rapid pace. We continue to explore new
          markets and maintain current ones. We strive to give the best every time and are able to
          do that, thanks to our experience and good relationship with suppliers all over Thailand,
          making it possible for us to provide the best Thai products at reasonable rates.
        </p>

        <div class="row fadeVisible">
          <div id="company-info" class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
            <table>
              <tr>
                <td width="30%">Business Type</td>
                <td width="70%">Manufacturer, Exporter</td>
              </tr>

              <tr>
                <td>Year Established</td>
                <td>1998</td>
              </tr>

              <tr>
                <td>Main Markets</td>
                <td>Asia, Africa, Australia, East Europe, West Europe, Middle East, North America, South America</td>
              </tr>

              <tr>
                <td>Annual Sales</td>
                <td>over USD 10,000,000</td>
              </tr>

              <tr>
                <td>Total Employees</td>
                <td>30-40 People</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
