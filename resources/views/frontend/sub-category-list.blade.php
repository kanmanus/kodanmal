@extends('frontend.layout.main-layout')

@section('title', ' - ' . $category->title)

@section('css')
  {!! Html::style('css/frontend/sub-category-list.css') !!}
@endsection

@section('content')
  <div class="content">
    <div class="container">
      <div class="category-name">{{ $category->title }}</div>

      <div class="sub-category-list">
        <div class="row">
          @foreach ($sub_categories as $key => $sub_category)
          <div class="col-sm-4">
            <a href="{{ URL::route('sub-category-detail', $sub_category->category_id) }}" class="sub-category">
              <div class="sub-category-img" style="background-image: url('{{ URL::asset('uploads/category/' . $sub_category->img_name) }}');"></div>
              <div class="sub-category-name">{{ $sub_category->title }}</div>
            </a>
          </div>
          @if (($key+1)%3 == 0)
        </div>
        <div class="row">
          @endif
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endsection
