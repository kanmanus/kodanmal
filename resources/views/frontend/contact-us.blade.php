@extends('frontend.layout.main-layout')

@section('title', ' - Contact Us')

@section('css')
  {!! Html::style('assets/fancybox/dist/css/jquery.fancybox.css') !!}
  {!! Html::style('css/frontend/contact-us.css') !!}
@endsection

@section('content')
  <div id="banner-top">
    <div class="cover"></div>
    <div class="text-center fadeVisible">
      Contact <span class="strong">Us</span>
    </div>
  </div>

  <div class="content">
    <div class="container">
      <div class="col-sm-6">
        <div class="paragraph fadeVisible">
          Not only kodanmal can offer sourcing quality products services for your requirement,
          but we can also offer you premiere packing, shipping, freight and cargo services from
          Thailand to your desired destination.
        </div>

        <div class="paragraph fadeVisible">
          For further enquiries, please contact and/or visit us at
        </div>

        <div class="paragraph address fadeVisible">
          <img src="{{ URL::asset('images/logo.png') }}" class="logo" alt="Kodanmal">
          <div class="strong">KODANMAL GROUP CO., LTD.</div>
          158/2-4 Sukhumvit Road, Soi 33<br>
          Klongton Nua, Wattana<br>
          Bangkok 10110, Thailand
        </div>

        <div class="paragraph more-info fadeVisible">
          <span class="strong">Tel:</span> +66(0)2 662 2478, +66(0)2 662 2481<br>
          <span class="strong">Fax:</span> +66(0)2 259 3886<br>
          <span class="strong">Email:</span> <a href="mailto:info@kodanmal.com">info@kodanmal.com</a>
        </div>
      </div>

      <div class="col-sm-6">
        <a href="{{ URL::asset('images/map.jpg') }}" class="fancybox fadeVisible">
          <img src="{{ URL::asset('images/map-thumb.jpg') }}" class="map">
        </a>
      </div>
    </div>
  </div>
@endsection

@section('script')
  {!! Html::script('assets/fancybox/dist/js/jquery.fancybox.pack.js') !!}
  <script>
  $(function(){
    $(".fancybox").fancybox();
  });
  </script>
@endsection
