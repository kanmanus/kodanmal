@extends('frontend.layout.main-layout')

@section('title', ' - Exhibitions')

@section('css')
  {!! Html::style('assets/bxslider/dist/jquery.bxslider.css') !!}
  {!! Html::style('css/frontend/exhibitions.css') !!}
@endsection

@section('content')
  <div id="banner-top">
    <div class="container">
      <ul class="bxSlider fadeVisible">
        @foreach ($banners as $banner)
        <li><img src="{{ URL::asset('uploads/exhibitions/' . $banner) }}"></li>
        @endforeach
      </ul>
    </div>
  </div>

  <div class="content">
    <div class="container">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="headline text-center fadeVisible">
          Exhibitions
        </div>

        <div class="detail fadeVisible">
          <p>
            Kodanmal Group will be attending the following trade shows. Please make an appointment for your
            visit via <a href="mailto:info@kodanmal.com">e-mail</a>.
          </p>

          <p>The trade show that Kodanmal will exhibit are as follows;</p>

          <p>
            @foreach ($exhibitions as $key => $exhibition)
            <div class="row">
              <div class="topic col-xs-6 col-sm-5 col-md-4">{{ $key+1 }}. {{ $exhibition->exhibition_name }}</div>
              <div class="detail col-xs-6 col-sm-7 col-md-8">{{ $exhibition->exhibition_date }}</div>
            </div>
            @endforeach
          </p>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  {!! Html::script('assets/bxslider/dist/jquery.bxslider.min.js') !!}
  <script>
  $(function(){
    $('.bxSlider').bxSlider({ mode: 'fade', auto: 'true' });
  });
  </script>
@endsection
