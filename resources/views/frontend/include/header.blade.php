<header>
  <div class="container">
    <a href="{{ URL::route('home') }}" id="logo"><img src="{{ URL::asset('images/logo.png') }}"></a>

    <nav>
      <div class="nav-toggle-icon">
        <i class="glyphicon glyphicon-menu-hamburger"></i>
      </div>

      <div class="nav-list">
        <a href="{{ URL::route('home') }}" class="nav-item text-center">
          <span class="sub-text"></span><br>HOME
        </a>
        <a href="#" class="nav-item">
          <span class="sub-text">CANNED</span><br>SEAFOOD
        </a>
        <a href="#" class="nav-item">
          <span class="sub-text">CANNED</span><br>VEGETABLES
        </a>
        <a href="#" class="nav-item">
          <span class="sub-text">CANNED</span><br>FRUITS
        </a>
        <a href="#" class="nav-item text-center">
          <span class="sub-text"></span><br>COCONUT
        </a>
        <a href="#" class="nav-item text-center">
          <span class="sub-text"></span><br>DRINKS
        </a>
        <a href="#" class="nav-item text-center">
          <span class="sub-text"></span><br>THAI RICE
        </a>
        <a href="#" class="nav-item text-center">
          <span class="sub-text"></span><br>COFFEE
        </a>
        <a href="#" class="nav-item text-center">
          <span class="sub-text"></span><br>SPICES
        </a>
        <a href="#" class="nav-item">
          <span class="sub-text">GENERAL</span><br>PRODUCTS
        </a>
        <a href="#" class="nav-item text-center">
          <span class="sub-text"></span><br>FMCG
        </a>
        <a href="{{ URL::route('category-list') }}" class="nav-item text-center">
          <span class="sub-text"></span><br>MORE
        </a>
      </div>
    </nav>
  </div>
</header>
