<footer>
  <div class="container">
    <div id="footer-nav">
      <a href="{{ URL::route('about-kodanmal') }}" class="nav-item">About KODANMAL</a>
      <a href="{{ URL::route('exhibitions') }}" class="nav-item">Exhibitions</a>
      <a href="{{ URL::route('contact-us') }}" class="nav-item">Contact Us</a>
      <a href="http://regalthai.com" target="_blank" class="nav-item">Regal Thai</a>
      <a href="http://kodanmalcorp.com" target="_blank" class="nav-item">Kodanmal Corp</a>
    </div>

    <div id="copyright">
      Copyright &copy; Kodanmal 2016
    </div>
  </div>
</footer>
