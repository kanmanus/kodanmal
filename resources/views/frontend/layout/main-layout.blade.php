<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{{ Config::get('constants.APP_NAME') }}@yield('title')</title>

    <!-- Responsive Window -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Custom fonts -->
    {!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
    {!! Html::style('https://fonts.googleapis.com/css?family=Questrial:400') !!}
    {!! Html::style('assets/animate/animate.css') !!}

    <!-- CSS Files -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('css/frontend/main.css') !!}
    @yield('css')
  </head>

  <body>
    @include('frontend.include.header')

    <div id="ui-main">
      @yield('content')
    </div>

    @include('frontend.include.footer')

    {!! Html::script('assets/jquery/jquery.min.js') !!}
    {!! Html::script('assets/viewportchecker/jquery.viewportchecker.min.js') !!}
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('js/frontend/global.js') !!}

    @yield('script')
  </body>
</html>
