@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
  <div id="banner-top">
    <div class="container">
      <div class="text fadeVisible">
        <div class="sub-text">Your Partner in</div>
        Food Industry
      </div>
    </div>
  </div>

  <div id="best-selling-products">
    <div class="container">
      <div class="promo-text fadeVisible">
        CHECK OUT OUR <span class="strong">BEST SELLING PRODUCTS</span>
      </div>

      <div class="product-list">
        @for ($i=0; $i<3; $i++)
        <div class="row">
          <a href="#" class="product-item col-sm-4 fadeVisible">
            <div class="product-img">
              <img src="{{ URL::asset('images/products/canned-tuna.png') }}" alt="Canned Tuna" class="img-responsive">
            </div>

            <div class="product-name">
              Canned Tuna
            </div>
          </a>

          <a href="#" class="product-item col-sm-4 fadeVisible">
            <div class="product-img">
              <img src="{{ URL::asset('images/products/canned-sardines.png') }}" alt="Canned Sardines & Mackerel" class="img-responsive">
            </div>

            <div class="product-name">
              Canned Sardines & Mackerel
            </div>
          </a>

          <a href="#" class="product-item col-sm-4 fadeVisible">
            <div class="product-img">
              <img src="{{ URL::asset('images/products/basil-seed-drink.png') }}" alt="Basil Seed Drink" class="img-responsive">
            </div>

            <div class="product-name">
              Basil Seed Drink
            </div>
          </a>
        </div>
        @endfor
      </div>
    </div>
  </div>
@endsection
