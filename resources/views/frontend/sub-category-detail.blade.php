@extends('frontend.layout.main-layout')

@section('title', ' - ' . $sub_category->title)

@section('css')
  {!! Html::style('css/frontend/sub-category-detail.css') !!}
@endsection

@section('content')
  <div class="content">
    <div class="container">
      <div class="category-name">{{ $category->title }}</div>
      <div class="sub-category-name">{{ $sub_category->title }}</div>

      <div class="description">
        {!! $sub_category->description !!}
      </div>

      <div class="product-list">
        <div class="row">
          @foreach ($products as $key => $product)
          <div class="col-sm-4">
            <div class="product">
              <div class="product-img" style="background-image: url('{{ URL::asset('uploads/product/' . $product->img_name) }}');"></div>
              <div class="product-name">{{ $product->title }}</div>
            </div>
          </div>
          @if (($key+1)%3 == 0)
        </div>
        <div class="row">
          @endif
          @endforeach
        </div>
      </div>

      <div class="more-description">
        {!! $sub_category->more_description !!}
      </div>
    </div>
  </div>
@endsection
