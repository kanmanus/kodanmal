@extends('frontend.layout.main-layout')

@section('title', ' - Our Categories')

@section('css')
  {!! Html::style('css/frontend/category-list.css') !!}
@endsection

@section('content')
  <div id="banner-top">
    <div class="text-center fadeVisible">
      Our <span class="strong">Categories</span>
    </div>
  </div>

  <div class="content">
    <div class="container">
      @foreach($categories as $category)
      <div class="category">
        <a href="{{ URL::route('sub-category-list', $category->category_id) }}" class="category-name">{{ $category->title }}</a>

        @if (count($category->sub_categories) > 0)
        <div class="sub-category-list">
          @foreach ($category->sub_categories as $sub_category)
          <div class="sub-category">
            <a href="{{ URL::route('sub-category-detail', $sub_category->category_id) }}">{{ $sub_category->title }}</a>
          </div>
          @endforeach
        </div>
        @endif
      </div>
      @endforeach
    </div>
  </div>
@endsection
